#!/usr/bin/env python
# encoding: utf-8

import os
import sys
from subprocess import Popen
import psutil

home_path = os.path.expanduser("~")
base_path = os.path.join(home_path, "code")
gen_file = os.path.join(home_path, "tools/code2ebook/gen.sh")
tmp_dir = os.path.join(home_path, "tmp/ebook")


class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def kill(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.get_children(recursive=True):
        proc.kill()
        process.kill()


def shell(command):
    # The os.setsid() is passed in the argument preexec_fn so
    # it's run after the fork() and before  exec() to run the shell.
    try:
        process = Popen(
            args=command,
            stdout=sys.stdout,
            shell=True,
            preexec_fn=os.setsid)
        return process.communicate()[0]
    except KeyboardInterrupt:
        print "\n==== " + repr(process)
        kill(process.pid)


def worker(info):
    branch = info['branch'] or 'master'
    shell("git subtree pull --squash --prefix=%s %s %s" %
          (info['path'], info['remote'], branch))


def _worker(info):
    title = info.get('title', info['path'].split('/')[-1])
    pdf_name = "%s.pdf" % title.replace('-', '_')

    with cd(tmp_dir):
        shell('output=$(ls -l | grep {0}); if [ -z "$output" ]; then echo {0}; \
              fi'.format(pdf_name))

jobs = [
    {'path': 'cool/blood', 'remote': 'https://github.com/logological/blood'},
    {'path': 'cv/Awesome-CV',
     'remote': 'https://github.com/posquit0/Awesome-CV.git'},
]


_jobs = [
    {'path': "lang/cpython"},
]


def main():
    import multiprocessing
    PROCESSES = 4
    pool = multiprocessing.Pool(processes=PROCESSES)

    TEST = True
    if TEST != "1":
        TEST = False
    if TEST:
        pool.map(_worker, jobs)
        pool.close()
        pool.join()
    else:
        try:
            pool.map_async(worker, _jobs)
            pool.close()
            pool.join()
        except KeyboardInterrupt:
            print "==== Main terminate!"
            kill(os.getpid())

    print "Total %d items converted!" % len(jobs)
    shell("ls -l ~/tmp/ebook | grep rw | wc -l")


if __name__ == '__main__':
    main()
